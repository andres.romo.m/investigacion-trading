# Import libraries
from enum import Enum
import pandas as pd
import numpy as np
import pandas_ta as ta
from mealpy import Problem
from abc import ABC, abstractmethod
import matplotlib.pyplot as plt

# ==================================
# ======== ENUMS ===================
# ==================================

class STATES(Enum):
    IN_LONG = 1
    IN_SHORT = 2
    WAITING = 3

class ACTIONS(Enum):
    OPEN_LONG = 1
    OPEN_SHORT = 2
    CLOSE_POS = 3
    NONE = 4

# ==================================
# ====== ESTRATEGIA BASE ===========
# ==================================

class EstrategiaBase(Problem,ABC):
    def __init__(self, symbol, tempo, len_sim , bounds = None, minmax = "min", strategy_name = "Estrategy",micro = False, **kwargs):
        self.strategy_name = strategy_name
        self.len_sim = len_sim
        self.micro = micro
        self.data = pd.read_csv(f'./{symbol}-{tempo}.csv')
        self.dataset_subset = False
        self.dataset_range = [0,-1]
        self.returns_history = []
        super().__init__(bounds, minmax, **kwargs)


    @abstractmethod
    def get_dataset(self, x):
        pass

    @abstractmethod
    def amend_position(self, x, bounds):
        pass

    @abstractmethod
    def open_position(self, indicadores, x):
        pass

    def set_dataset_subset(self, subset):
        self.dataset_subset = subset

    def restart_returns_history(self):
        self.returns_history = []

    def get_datset_index(self):
        len_dataset = 0
        non_nan = int( self.ub.max() )
        len_data = len(self.data) - non_nan
        i = 0
        while (i+self.len_sim) < len_data:
            i += int(self.len_sim/2)
            len_dataset += 1
        return np.array(list(range(len_dataset)))

    def split_dataset(self, data):
        dataset = []
        i = 0
        while (i+self.len_sim) < data.shape[1]:
            dataset.append( data[: , i:i+self.len_sim] )
            i += int(self.len_sim/2)

        return np.array(dataset)

    def obj_func(self, x):
        self.restart_returns_history()
        dataset = self.get_dataset(x)
        if type(self.dataset_subset) == np.ndarray:
            dataset = dataset[self.dataset_subset]
        fitness = np.array([ self.simulacion(d,x) for d in dataset ])
        mean = fitness.mean()
        std = np.std(self.returns_history) if len(self.returns_history) else 1
        return mean,std

    def performance_graph(self, x):
        complete_serie = self.get_dataset(x,option_graph=True)
        historial_capital = self.simulacion(complete_serie,x,option_graph=True)

        plt.xlabel('Time')
        plt.ylabel('Accumulated capital')
        plt.gca().get_xaxis().set_ticks([])
        plt.plot(historial_capital[:,1],historial_capital[:,0])
        plt.show()

    def simulacion(self,serie, x, option_graph = False):
        # ------------------------------------------ Variables de la simulacion
        money = 1
        entry_price = 0
        trading_state = STATES.WAITING
        max_long = serie[0,0]
        min_short = serie[0,0]
        historial_capital = []

        # MICRO
        returns = []

        # ------------------------------------------ Variables de x para cerrar posicion
        stop_long = x[-4]
        stop_short = x[-3]
        take_profit_long = x[-2]
        take_profit_short = x[-1]

        # ------------------------------------------- LOOP
        for i in range(serie.shape[1]):
            price = serie[0,i]
            indicadores = serie[:,i]

            # ======= WAITING =========
            if trading_state == STATES.WAITING:
                action = self.open_position(indicadores,x)

                if action == ACTIONS.OPEN_LONG:                         # OPEN LONG POSITION
                    entry_price = price
                    max_long = price
                    stop_win = entry_price*take_profit_long
                    trading_state = STATES.IN_LONG

                elif action == ACTIONS.OPEN_SHORT:                      # OPEN SHORT POSITION
                    entry_price = price
                    min_short = price
                    stop_win = entry_price*take_profit_short
                    trading_state = STATES.IN_SHORT

            # ======= IN_LONG =========
            elif trading_state == STATES.IN_LONG:
                # move stop_loss stop
                if price>max_long:  max_long = price

                if (price < max_long*stop_long) or (price>stop_win):    # CLOSE POSITION
                    exit_price = price
                    pnl = ( (1/entry_price) - (1/exit_price) ) * money * exit_price
                    opening_fee = money*0.0004
                    exit_fee = (money/entry_price)*exit_price*0.0004
                    returns.append(pnl-opening_fee-exit_fee)
                    money += pnl-opening_fee-exit_fee

                    # next state
                    trading_state = STATES.WAITING


            # ======= IN_SHORT =========
            elif trading_state == STATES.IN_SHORT:
                # move stop_loss stop
                if price < min_short: min_short=price
                # close_pos
                if (price > min_short*stop_short) or (price<stop_win):
                    exit_price = price
                    pnl = ( (1/exit_price) - (1/entry_price) ) * money * exit_price
                    opening_fee = money*0.0004
                    exit_fee = (money/entry_price)*exit_price*0.0004
                    returns.append(pnl-opening_fee-exit_fee)
                    money += pnl-opening_fee-exit_fee

                    # next state
                    trading_state = STATES.WAITING

            historial_capital.append((money,i))

        self.returns_history = self.returns_history+returns

        if option_graph:            return np.array( historial_capital )

        if self.micro:
            if not len(returns):    return -1
            else:                   return np.mean(returns)
        else:
            return money

# ==================================
# =========== RSI_2SMA =============
# ==================================

class RSI_2SMA(EstrategiaBase):

    def get_dataset(self,x, option_graph = False):
        len_rsi = x[0]
        len_sma_fast = x[3]
        len_sma_slow = x[4]

        data = self.data['close']
        close = data.to_numpy().reshape(1,-1)
        rsi = ta.rsi(data,len_rsi).to_numpy().reshape(1,-1)
        sma_fast = ta.sma(data,len_sma_fast).to_numpy().reshape(1,-1)
        sma_slow = ta.sma(data,len_sma_slow).to_numpy().reshape(1,-1)
        data = np.concatenate([close,rsi,sma_fast,sma_slow],axis=0)

        non_nan = int( self.ub.max() )
        data = data[:,non_nan:]

        if option_graph:        return data
        else:                   return self.split_dataset(data)

    def amend_position(self, x, bounds):
        lb = bounds["lb"]
        # TIPO DE DATO
        x[0] = int(x[0])      # LEN_RSI
        x[1] = int(x[1])      # OVERSOLD_THRES
        x[2] = int(x[2])      # OVERBOUGHT_THRES
        x[3] = int(x[3])      # LEN_SMA_FAST
        x[4] = int(x[4])      # LEN_SMA_SLOW
        x[5] = round(x[5],3)  # STOP_LONG
        x[6] = round(x[6],3)  # STOP_SHORT
        x[7] = round(x[7],3)  # TAKE_PROFIT_LONG
        x[8] = round(x[8],3)  # TAKE_PROFIT_SHORT

        # RESTRICCIONES
        if x[3]>=x[4]:  x[3] = np.random.randint(lb[3],x[4])
        return x

    def open_position(self, indicadores, x):
        rsi = indicadores[1]
        sma_fast = indicadores[2]
        sma_slow = indicadores[3]
        OVERSOLD_THRES = x[1]
        OVERBOUGHT_THRES = x[2]

        if (rsi<OVERSOLD_THRES) and (sma_fast>sma_slow):        ret = ACTIONS.OPEN_LONG
        elif (rsi>OVERBOUGHT_THRES) and (sma_fast<sma_slow):    ret = ACTIONS.OPEN_SHORT
        else:                                                   ret = ACTIONS.NONE

        return ret

# ==================================
# ============= _2SMA ==============
# ==================================

class WAITING_STATES(Enum):
    WAITING_DOWN = 1
    WAITING_UP = 2
    POS_OPEN = 3

class SMA2(EstrategiaBase):
    def __init__(self, symbol, tempo, len_sim, bounds = None, minmax = "min", strategy_name="Estrategy", micro=False, **kwargs):
        self.WAITING_STATE = WAITING_STATES.WAITING_DOWN

        super().__init__(symbol, tempo, len_sim , bounds, minmax, strategy_name, micro,**kwargs)

    def get_dataset(self, x, option_graph = False):
        len_sma_fast = x[0]
        len_sma_slow = x[1]
        data = self.data['close']
        close = data.to_numpy().reshape(1,-1)
        sma_fast = ta.sma(data,len_sma_fast).to_numpy().reshape(1,-1)
        sma_slow = ta.sma(data,len_sma_slow).to_numpy().reshape(1,-1)
        data = np.concatenate([close,sma_fast,sma_slow],axis=0)

        non_nan = int( self.ub.max() )
        data = data[:,non_nan:]

        if option_graph:        return data
        else:                   return self.split_dataset(data)

    def amend_position(self, x, bounds):
        lb = bounds["lb"]
        # TIPO DE DATO
        x[0] = int(x[0])      # LEN_SMA_FAST
        x[1] = int(x[1])      # LEN_SMA_SLOW
        x[2] = round(x[2],3)  # STOP_LONG
        x[3] = round(x[3],3)  # STOP_SHORT
        x[4] = round(x[4],3)  # TAKE_PROFIT_LONG
        x[5] = round(x[5],3)  # TAKE_PROFIT_SHORT

        # RESTRICCIONES
        if x[0]>=x[1]:  x[0] = np.random.randint(lb[0],x[1])
        return x

    def open_position(self, indicadores, x):
        sma_fast = indicadores[1]
        sma_slow = indicadores[2]

        if self.WAITING_STATE==WAITING_STATES.POS_OPEN:
            if sma_fast >= sma_slow:     self.WAITING_STATE = WAITING_STATES.WAITING_UP
            elif sma_fast< sma_slow:     self.WAITING_STATE = WAITING_STATES.WAITING_DOWN

        if (self.WAITING_STATE==WAITING_STATES.WAITING_DOWN) and (sma_fast>sma_slow):
            self.WAITING_STATE = WAITING_STATES.POS_OPEN
            return ACTIONS.OPEN_LONG

        elif (self.WAITING_STATE==WAITING_STATES.WAITING_UP) and (sma_fast<sma_slow):
            self.WAITING_STATE = WAITING_STATES.POS_OPEN
            return ACTIONS.OPEN_SHORT

        else:
            return ACTIONS.NONE

# ==================================
# ========== MACD_ADX ==============
# ==================================

class MACD_ADX(EstrategiaBase):

    def get_dataset(self, x, option_graph = False):
        len_adx = x[0]
        len_macd_fast = x[2]
        len_macd_slow = x[3]
        len_macd_signal = x[4]

        close = self.data['close'].to_numpy().reshape(1,-1)
        macd = ta.macd(self.data['close'],len_macd_fast,len_macd_slow,len_macd_signal)
        adx = ta.adx(self.data['high'],self.data['low'],self.data['close'],len_adx)

        macd_signal = macd[macd.columns[2]].to_numpy().reshape(1,-1)
        adx = adx[adx.columns[0]].to_numpy().reshape(1,-2)

        data = np.concatenate([close,macd_signal,adx],axis=0)

        non_nan = int( self.ub.max() )
        data = data[:,non_nan:]

        if option_graph:        return data
        else:                   return self.split_dataset(data)

    def amend_position(self, x, bounds):
        lb = bounds["lb"]
        # TIPO DE DATO
        x[0] = int(x[0])      # LEN_ADX
        x[1] = int(x[1])      # ADX_STRONG
        x[2] = int(x[2])      # LEN_MACD_FAST
        x[3] = int(x[3])      # LEN_MACD_SLOW
        x[4] = int(x[4])      # LEN_MACD_SIGNAL
        x[5] = round(x[5],3)  # STOP_LONG
        x[6] = round(x[6],3)  # STOP_SHORT
        x[7] = round(x[7],3)  # TAKE_PROFIT_LONG
        x[8] = round(x[8],3)  # TAKE_PROFIT_SHORT

        # RESTRICCIONES
        if x[2]>=x[3]:  x[2] = np.random.randint(lb[2],x[3])
        if x[4]>=x[2]:  x[4] = np.random.randint(lb[4],x[2])
        return x

    def open_position(self, indicadores, x):
        macd_signal = indicadores[1]
        adx = indicadores[2]

        ADX_STRONG = x[1]

        if adx>ADX_STRONG and macd_signal>0:        return ACTIONS.OPEN_LONG
        if adx>ADX_STRONG and macd_signal<0:        return ACTIONS.OPEN_SHORT
        return ACTIONS.NONE

# ==================================
# ======== BOLLINGER_RSI ===========
# ==================================

class BOLLINGER_RSI(EstrategiaBase):

    def get_dataset(self, x, option_graph = False):
        len_bb = x[0]
        bb_std_upper = x[1]
        bb_std_lower = x[2]
        len_rsi = x[3]

        data = self.data['close']
        close = data.to_numpy().reshape(1,-1)
        bb_upper = ta.bbands(data,len_bb,bb_std_upper)
        bb_lower = ta.bbands(data,len_bb,bb_std_lower)
        bb_central = bb_upper[bb_upper.columns[1]].to_numpy().reshape(1,-1)
        bb_upper = bb_upper[bb_upper.columns[2]].to_numpy().reshape(1,-1)
        bb_lower = bb_lower[bb_lower.columns[0]].to_numpy().reshape(1,-1)
        rsi = ta.rsi(data,len_rsi).to_numpy().reshape(1,-1)
        data = np.concatenate([close,bb_central,bb_upper,bb_lower,rsi],axis=0)

        non_nan = int( self.ub.max() )
        data = data[:,non_nan:]

        if option_graph:        return data
        else:                   return self.split_dataset(data)

    def amend_position(self, x, bounds):
        # TIPO DE DATO
        x[0] = int(x[0])        # LEN_BB
        x[1] = round(x[1],3)    # BB_STD_UPPER
        x[2] = round(x[2],3)    # BB_STD_LOWER
        x[3] = int(x[3])        # LEN_RSI
        x[4] = int(x[4])        # OVERSOLD_THRES
        x[5] = int(x[5])        # OVERBOUGHT_THRES
        x[6] = round(x[6],3)    # STOP_LONG
        x[7] = round(x[7],3)    # STOP_SHORT
        x[8] = round(x[8],3)    # TAKE_PROFIT_LONG
        x[9] = round(x[9],3)    # TAKE_PROFIT_SHORT

        # RESTRICCIONES
        return x

    def open_position(self, indicadores, x):
        value = indicadores[0]
        bb_central = indicadores[1]
        bb_upper = indicadores[2]
        bb_lower = indicadores[3]
        rsi = indicadores[4]

        OVERSOLD_THRES = x[4]
        OVERBOUGHT_THRES = x[5]

        if value<bb_lower and rsi<OVERSOLD_THRES:       return ACTIONS.OPEN_LONG
        elif value>bb_upper and rsi>OVERBOUGHT_THRES:   return ACTIONS.OPEN_SHORT
        else:                                           return ACTIONS.NONE