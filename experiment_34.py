# Import libraries
import pandas_ta as ta
import numpy as np
import matplotlib.pyplot as plt
import pickle
import time
from sklearn.model_selection import train_test_split
from mealpy.swarm_based.WOA import OriginalWOA
from mealpy.math_based.SCA import OriginalSCA
from mealpy.evolutionary_based.GA import BaseGA
from mealpy import FloatVar
from mealpy.utils.visualize import *
from estrategias import *

# =====================================
# ======== DEFINE STRATEGIES ==========
# =====================================

symbol = 'BTCUSDT'
tempo = '5m'
len_sim = 2*30*24*12  # 2meses x 30dias x 24hrs x 12mediciones/hr
obj_weights = [1,-1/10]

# MACD_ADX
# X = [ LEN_ADX ADX_STRONG LEN_MACD_FAST LEN_MACD_SLOW LEN_MACD_SIGNAL STOP_LONG STOP_SHORT TAKE_PROFIT_LONG TAKE_PROFIT_SHORT ]
bound_macd = FloatVar(lb = [5,   10, 5,   6,   4,   0.90,  1.001, 1.001, 0.90], ub = [288, 75, 288, 288, 288, 0.999, 1.1,   1.1,   0.999]) # 288 = 3 dias
macd_adx = MACD_ADX(symbol, tempo, len_sim, bounds = bound_macd, minmax = "max", strategy_name = 'macd_adx', obj_weights = obj_weights)

# BOLLINGER_RSI
# X = [LEN_BB BB_STD_UPPER BB_STD_LOWER LEN_RSI OVERSOLD_THRES OVERBOUGHT_THRES STOP_LONG STOP_SHORT TAKE_PROFIT_LONG TAKE_PROFIT_SHORT ]
bound_bollinger = FloatVar(lb = [5,   0.5, 0.5, 5,   25, 51,  0.90,  1.001, 1.001, 0.90], ub = [288, 3,   3,   288, 49, 75, 0.999, 1.1,   1.1,   0.999]) # 288 = 3 dias
bollinger_rsi = BOLLINGER_RSI(symbol, tempo, len_sim, bounds = bound_bollinger, minmax = "max", strategy_name = 'bollinger_rsi', obj_weights = obj_weights)

# =====================================
# ======== RUN EXPERIMENT =============
# =====================================

n_ejecuciones = 31
estrategias = [macd_adx, bollinger_rsi]
optimizer = BaseGA(60,40,pm=0.05)
results = []

# Files
with open('logs/macd_adx.log', 'wb') as file:
  pickle.dump(results, file)

with open('logs/bollinger_rsi.log', 'wb') as file:
  pickle.dump(results, file)

for estrategia in estrategias:

    with open('logs/logs34.log','a') as file:
        print('====================================',file=file)
        print(f'STARTING STRATEGY {estrategia.strategy_name}',file=file)
        print('====================================',file=file)

    # RUN 30 TIMES
    for i in range(n_ejecuciones):
        # OPTIMIZE
        time_start = time.time()
        estrategia.micro = True
        list_index = estrategia.get_datset_index()
        index_train, index_test = train_test_split(list_index,random_state=3)
        estrategia.set_dataset_subset(index_train)
        optimizer.solve(estrategia)
        time_end = time.time() - time_start
        best_position = optimizer.g_best.solution
        best_fitness = optimizer.g_best.target.fitness

        with open('logs/logs34.log','a') as file:
            print(f'{estrategia.strategy_name} -> iter={i} ; fitness={best_fitness}',file=file)


        # SAVE EXPERIMIENT DATA
        resultados_estrategia = {
            'current_optimizer':estrategia.strategy_name,
            'current_ejecution':i,
            'current_position':best_position,
            'current_history':optimizer.history,
            'current_fitness':best_fitness,
            'current_time': time_end,
        }

        file_estrategy = 'logs/macd_adx.log' if estrategia.strategy_name == "macd_adx" else 'logs/bollinger_rsi.log'

        with open(file_estrategy, 'rb') as file:
            current_results = pickle.load(file)

        current_results.append(resultados_estrategia)

        with open(file_estrategy, 'wb') as file:
            pickle.dump(current_results, file)